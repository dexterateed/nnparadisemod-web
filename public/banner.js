//images for banner
var images = [
    'categories/world/biomes/cold_rocky_desert.png',
    'categories/world/biomes/glacier.png',
    'categories/world/biomes/rocky_desert.png',
    'categories/world/biomes/salt_flat.png',
    'categories/world/biomes/snowy_rocky_desert.png',
    'categories/world/biomes/temperate_rainforest.png',
    'categories/world/biomes/volcanic_field.png',
    'categories/world/biomes/volcanic_mountains.png',
    'categories/world/dimensions/deep_underground.png',
    'categories/world/dimensions/deep_void.png',
    'categories/world/underground/cave_crystal.png',
    'categories/world/underground/cave_desert.png',
    'categories/world/underground/cave_icy_desert.png',
    'categories/world/underground/cave_icy.png',
    'categories/world/underground/cave_jungle.png',
    'categories/world/underground/cave_mesa.png',
    'categories/world/underground/cave_normal.png',
    'categories/world/underground/cave_ocean.png',
    'categories/world/underground/cave_rocky.png',
    'categories/world/underground/cave_swamp.png',
    'categories/world/underground/salt_cave.png'
];
    
//add element to banner header
$('<img id="banner" src="' + images[Math.floor(Math.random() * images.length)] + '">').appendTo('#rnd_banner'); 
